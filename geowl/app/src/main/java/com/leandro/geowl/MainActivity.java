package com.leandro.geowl;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public final static String EXTRA_JSON = "com.wakeme.MESSAGE";
    public final static String EXTRA_INDEX = "com.wakeme.INDEX";
    private ArrayList<Alarm> alarms = new ArrayList<>();
    private AlarmAdapter alarmAdapter;
    private AlertDialog.Builder builder;
    private EditText input;
    private ViewHolder viewholder;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private String sharedString;
    private JSONObject sharedJson;
    private JSONArray sharedArray;

    private class ViewHolder{
        FloatingActionButton fab;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(viewholder==null){
            viewholder = new ViewHolder();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewholder.fab = (FloatingActionButton) findViewById(R.id.fab);
        viewholder.fab.setOnClickListener(fablistener);

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar alarma");
        input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", positiveButtonListener);
        builder.setNegativeButton("Cancel", negativeButtonListener);

        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/

        alarmAdapter = new AlarmAdapter(this,alarms);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(alarmAdapter);

        listView.setOnItemLongClickListener(longListener);
        listView.setOnItemClickListener(shortListener);

        refreshData();

        askPermission();

        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if(android.os.Build.VERSION.SDK_INT >= 21) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    public void refreshData(){
        sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), getApplicationContext().MODE_PRIVATE);
        Log.w("SharedPref", sharedPref.toString());
        String defaultValue = getString(R.string.saved_json_default);
        sharedString = sharedPref.getString(getString(R.string.saved_json),defaultValue);
        editor = sharedPref.edit();
        
        Log.w("Main","Refresh " + sharedString);
        try{
            sharedJson = new JSONObject(sharedString);
            sharedArray = sharedJson.getJSONArray(getString(R.string.saved_json_array));
        } catch(JSONException e){
            sharedArray = new JSONArray();
        }

        alarms.clear();
        for(int i = 0; i < sharedArray.length(); i++){
            try{
                alarms.add(new Alarm(sharedArray.getJSONObject(i)));
            } catch(JSONException e) {
                throw new RuntimeException(e);
            }
        }
        Log.w("Main","Alarms " + alarms.toString());
        alarmAdapter.notifyDataSetChanged();
    }

    public void askPermission(){
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
            }, 55);
        } else {
            Intent serviceIntent = new Intent(getApplicationContext(), TrackingService.class);
            serviceIntent.putExtra(EXTRA_JSON, sharedString);
            Log.e("INTENT", sharedString);
            startService(serviceIntent);
        }
    }

    public DialogInterface.OnClickListener negativeButtonListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            input.setText("");
            dialog.cancel();
        }
    };

    public DialogInterface.OnClickListener positiveButtonListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if(input.getText().toString().length() > 0){
                Alarm auxAlarm = new Alarm(input.getText().toString());
                sharedArray.put(auxAlarm.jsonAlarm);
                try{
                    sharedJson.put(getString(R.string.saved_json_array), sharedArray);
                } catch(JSONException e){
                    throw new RuntimeException(e);
                }
                sharedString = sharedJson.toString();
                editor.clear();
                editor.putString(getString(R.string.saved_json), sharedString);
                editor.commit();

                Intent serviceIntent = new Intent(getApplicationContext(), TrackingService.class);
                serviceIntent.putExtra(EXTRA_JSON, sharedString);
                Log.e("INTENT", sharedString);
                startService(serviceIntent);

                refreshData();

                Snackbar.make(viewholder.fab, "Alarm added!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                input.setText("");
            } else {
                Snackbar.make(viewholder.fab, "You have to enter a title", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                input.setText("");
            }
        }
    };

    public View.OnClickListener fablistener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(input.getParent()!=null)
                ((ViewGroup)input.getParent()).removeView(input);

            builder.show();

            alarmAdapter.notifyDataSetChanged();
        }
    };


    public AdapterView.OnItemClickListener shortListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(MainActivity.this, AlarmActivity.class);
            intent.putExtra(EXTRA_JSON, sharedString);
            intent.putExtra(EXTRA_INDEX, position);
            startActivity(intent);
        }
    };

    public AdapterView.OnItemLongClickListener longListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Delete alarm")
                    .setMessage("Are you sure you want to delete this alarm?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            alarms.remove(position);
                            alarmAdapter.notifyDataSetChanged();
                            sharedArray.remove(position);
                            try{
                                sharedJson.put(getString(R.string.saved_json_array), sharedArray);
                            } catch(JSONException e){
                                throw new RuntimeException(e);
                            }
                            sharedString = sharedJson.toString();
                            editor.clear();
                            editor.putString(getString(R.string.saved_json), sharedString);
                            editor.commit();

                            Intent serviceIntent = new Intent(getApplicationContext(), TrackingService.class);
                            serviceIntent.putExtra(EXTRA_JSON, sharedString);
                            Log.e("INTENT", sharedString);
                            startService(serviceIntent);

                            Snackbar.make(viewholder.fab, "Alarm deleted!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return true;
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 55: {
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 55);
                }
                else
                {
                    Log.w("Main","Starting tracking service");
                    Intent serviceIntent = new Intent(getApplicationContext(), TrackingService.class);
                    serviceIntent.putExtra(EXTRA_JSON, sharedString);
                    Log.e("INTENT", sharedString);
                    startService(serviceIntent);
                }
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
