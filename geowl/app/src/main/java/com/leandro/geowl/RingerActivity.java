package com.leandro.geowl;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;


public class RingerActivity extends AppCompatActivity {

    private int alarmType;
    private int alarmVolume;
    private String alarmTone;
    private String alarmDirection;
    private ImageButton dismissButton;
    private Vibrator vibrator;
    private MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.w("RINGER", "On Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_ringer);

        Intent intent = getIntent();
        alarmType = intent.getIntExtra("type", 0);
        alarmVolume = intent.getIntExtra("volume", 0);
        alarmTone = intent.getStringExtra("tone");
        alarmDirection = intent.getStringExtra("direction");

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if(android.os.Build.VERSION.SDK_INT >= 21) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }

        dismissButton = (ImageButton) findViewById(R.id.dismiss_button);
        dismissButton.setOnClickListener(dismissListener);

        TextView alarmText = (TextView) findViewById(R.id.destination);
        alarmText.setText(alarmDirection);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        RingtoneManager manager = new RingtoneManager(this);
        manager.setType(RingtoneManager.TYPE_ALARM);
        Cursor cursor = manager.getCursor();
        while (cursor.moveToNext()) {
            if(cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX).equals(alarmTone)){
                notification = manager.getRingtoneUri(cursor.getPosition());
            }
        }

        player = MediaPlayer.create(getApplicationContext(), notification);

        float log1=(float)(Math.log(10-alarmVolume)/Math.log(10));
        player.setVolume(1-log1,1-log1);

        if(alarmType == 0){
            player.start();
        } else if(alarmType == 1){
            vibrator.vibrate(1000 * 60);
        } else {
            player.start();
            vibrator.vibrate(1000 * 60);
        }

    }

    public ImageButton.OnClickListener dismissListener = new ImageButton.OnClickListener() {
        @Override
        public void onClick(View v) {
            player.stop();
            player.release();
            vibrator.cancel();
            finish();
        }
    };

}
