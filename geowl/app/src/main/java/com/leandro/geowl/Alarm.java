package com.leandro.geowl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Alarm {
    public JSONObject jsonAlarm;

    public Alarm(String direction){
        try {
            //name, lat,lng, repeat, enabled, days[], volume, type(vibrate,sound), tone, distance
            JSONArray auxArray = new JSONArray();
            auxArray.put(1);
            auxArray.put(2);
            auxArray.put(3);
            auxArray.put(4);
            auxArray.put(5);
            jsonAlarm = new JSONObject();
            jsonAlarm.put("direction", direction);
            jsonAlarm.put("repeat", true);
            jsonAlarm.put("enabled", true);
            jsonAlarm.put("outside", true);
            jsonAlarm.put("days", auxArray);
            jsonAlarm.put("volume", 10);
            jsonAlarm.put("type", 0);
            jsonAlarm.put("tone", "TONE");
            jsonAlarm.put("distance", 300);
            jsonAlarm.put("latitude", 0.0);
            jsonAlarm.put("longitude", 0.0);
            jsonAlarm.put("start", 7);
            jsonAlarm.put("finish", 22);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public Alarm(JSONObject json){
        jsonAlarm = json;
    }

}
