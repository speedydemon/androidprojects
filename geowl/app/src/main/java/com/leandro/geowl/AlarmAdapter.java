package com.leandro.geowl;

import android.content.Context;
import android.media.Image;
import android.support.annotation.MainThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.util.ArrayList;


public class AlarmAdapter extends ArrayAdapter<Alarm>{

    public static class ViewHolder{
        TextView alarmDirection;
        TextView alarmDays;
        ImageView alarmEnabled;
    }

    public AlarmAdapter(Context context, ArrayList<Alarm> alarms) {
        super(context, 0, alarms);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Alarm alarm = getItem(position);

        ViewHolder viewholder;
        if(convertView == null){
            viewholder = new ViewHolder();

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);

            viewholder.alarmDirection = (TextView) convertView.findViewById(R.id.listDirection);
            viewholder.alarmDays = (TextView) convertView.findViewById(R.id.listDays);
            viewholder.alarmEnabled = (ImageView) convertView.findViewById(R.id.listIcon);

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }

        if (alarm.jsonAlarm.has("direction")) viewholder.alarmDirection.setText(alarm.jsonAlarm.optString("direction"));

        if (alarm.jsonAlarm.has("days")){
            String[] daysString = getContext().getResources().getStringArray(R.array.days_array);
            String daysMessage = "";
            try{
                if(alarm.jsonAlarm.getJSONArray("days").length() == 0){
                    daysMessage = "Never";
                } else if(alarm.jsonAlarm.getJSONArray("days").length() == 7) {
                    daysMessage = "Every day";
                } else {
                    JSONArray auxArray = alarm.jsonAlarm.getJSONArray("days");
                    daysMessage = daysMessage.concat(daysString[auxArray.getInt(0)]);
                    int arrayLength = auxArray.length();
                    for(int i = 1; i < arrayLength; i++){
                        daysMessage = daysMessage.concat(", ");
                        daysMessage = daysMessage.concat(daysString[auxArray.getInt(i)]);
                    }
                }
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            viewholder.alarmDays.setText(daysMessage);
        }

        if (alarm.jsonAlarm.has("enabled")){
            viewholder.alarmEnabled.setColorFilter(getContext().getResources().getColor(R.color.enabled));
        } else {
            viewholder.alarmEnabled.setColorFilter(getContext().getResources().getColor(R.color.disabled));
        }

        return convertView;
    }

}
