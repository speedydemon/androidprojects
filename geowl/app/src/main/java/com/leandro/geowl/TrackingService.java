package com.leandro.geowl;


import android.*;
import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

import android.os.PowerManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class TrackingService extends Service implements
        ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private Notification notification;
    private NotificationManager mNotificationManager;

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private String sharedString;
    private JSONObject sharedJson;
    private JSONArray sharedArray;
    private ArrayList<Alarm> alarms = new ArrayList<>();

    private PowerManager.WakeLock wl;

    protected static final String TAG = "location-updates";
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        if(intent != null && intent.getStringExtra(MainActivity.EXTRA_JSON) != "" && intent.getStringExtra(MainActivity.EXTRA_JSON) != null){
            sharedString = intent.getStringExtra(MainActivity.EXTRA_JSON);
            editor.clear();
            editor.putString(getString(R.string.saved_json), sharedString);
            editor.commit();
            Log.e(TAG, "updated " + sharedString);
        }
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        super.onCreate();

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        PowerManager pm = (PowerManager)getApplicationContext().getSystemService(
                getApplicationContext().POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "GPS");
        wl.acquire();

        sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), getApplicationContext().MODE_PRIVATE);
        editor = sharedPref.edit();

        createLocationRequest();
        buildGoogleApiClient();
    }


    protected synchronized void buildGoogleApiClient() {
        Log.e(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        Log.e(TAG, "creatingLocationRequest");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    protected void startLocationUpdates() {
        Log.e(TAG, "starting Location updates");
        if ( ContextCompat.checkSelfPermission( this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission( this.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.e(TAG, "Connected to GoogleApiClient");

        if (mCurrentLocation == null && ContextCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

        startLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "onLocationChanged " + location.toString());
        mCurrentLocation = location;

        if(location.getAccuracy() < 300){
            refreshData();

            boolean soundAlarm = false;

            Calendar calendar = Calendar.getInstance();
            int today = calendar.get(Calendar.DAY_OF_WEEK);

            Alarm alarm2Sound = new Alarm("Empty");

            for(int i = 0; i < alarms.size(); i++){
                Alarm aux = alarms.get(i);
                JSONArray daysArray;
                boolean todayIsTheDay = false;

                try{
                    daysArray = aux.jsonAlarm.getJSONArray("days");
                    for(int j = 0; j < daysArray.length(); j++){
                        if(today == (int)daysArray.get(j) + 1){
                            todayIsTheDay = true;
                        }
                    }
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

                if(todayIsTheDay){
                    Location alarmLocation = new Location("");
                    alarmLocation.setLatitude(aux.jsonAlarm.optDouble("latitude"));
                    alarmLocation.setLongitude(aux.jsonAlarm.optDouble("longitude"));
                    float distanceInMeters = mCurrentLocation.distanceTo(alarmLocation);
                    if(distanceInMeters > aux.jsonAlarm.optInt("distance") * 2){
                        updateRange2Out(i);
                    }
                    if(distanceInMeters < aux.jsonAlarm.optInt("distance")){
                        if(aux.jsonAlarm.optBoolean("outside")){
                            soundAlarm = true;
                        }

                        alarm2Sound = aux;
                        updateRange2In(i);
                    }
                }
            }

            if(soundAlarm){
                alarmRinger(alarm2Sound.jsonAlarm);
            }

        }

    }

    private void alarmRinger(JSONObject alarm){
        Intent ringer = new Intent(getBaseContext(), RingerActivity.class);
        ringer.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ringer.putExtra("direction", alarm.optString("direction"));
        ringer.putExtra("type", alarm.optInt("type"));
        ringer.putExtra("volume", alarm.optInt("volume"));
        ringer.putExtra("tone", alarm.optString("tone"));
        startActivity(ringer);
    }

    public void updateRange2Out(int index){
        try{
            Alarm aux = alarms.get(index);
            aux.jsonAlarm.put("outside", true);

            sharedArray.put(index, aux.jsonAlarm);
            sharedJson.put(getString(R.string.saved_json_array), sharedArray);
            sharedString = sharedJson.toString();

            editor.clear();
            editor.putString(getString(R.string.saved_json), sharedString);
            editor.commit();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateRange2In(int index){
        try{
            Alarm aux = alarms.get(index);
            aux.jsonAlarm.put("outside", false);

            sharedArray.put(index, aux.jsonAlarm);
            sharedJson.put(getString(R.string.saved_json_array), sharedArray);
            sharedString = sharedJson.toString();

            editor.clear();
            editor.putString(getString(R.string.saved_json), sharedString);
            editor.commit();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    public void buildNotification(String title, String context){
        notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_crosshairs_gps)
                .setContentTitle(title)
                .setContentText(context)
                .setOngoing(true)
                .build();
    }

    @Override
    public void onDestroy()
    {
        Log.e("GPS", "onDestroy");
        wl.release();
        stopLocationUpdates();
        super.onDestroy();
    }

    public void refreshData(){
        String defaultValue = getString(R.string.saved_json_default);
        sharedString = sharedPref.getString(getString(R.string.saved_json),defaultValue);

        try{
            sharedJson = new JSONObject(sharedString);
            sharedArray = sharedJson.getJSONArray(getString(R.string.saved_json_array));
        } catch(JSONException e){
            sharedArray = new JSONArray();
        }

        alarms.clear();

        for(int i = 0; i < sharedArray.length(); i++){
            try{
                alarms.add(new Alarm(sharedArray.getJSONObject(i)));
            } catch(JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

}