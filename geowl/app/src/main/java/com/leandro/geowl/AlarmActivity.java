package com.leandro.geowl;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class AlarmActivity extends AppCompatActivity implements OnMapReadyCallback {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private String sharedString;
    private JSONObject sharedJson;
    private JSONArray sharedArray;
    private JSONObject jsonAlarm;
    private ArrayList<CheckBox> dialogChecks;
    private int alarmPosition;

    private TextView alarmName;
    private TextView alarmDays;
    private TextView alarmType;
    private TextView alarmTone;
    private Button cancelButton;
    private Button doneButton;
    private String[] daysString;
    private String[] typeString;
    private DiscreteSeekBar distanceBar;
    private DiscreteSeekBar volumeBar;

    private Circle circle;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        sharedString = intent.getStringExtra(MainActivity.EXTRA_JSON);
        alarmPosition = intent.getIntExtra(MainActivity.EXTRA_INDEX, 0);
        try{
            sharedJson = new JSONObject(sharedString);
            sharedArray = sharedJson.getJSONArray(getString(R.string.saved_json_array));
            jsonAlarm = sharedArray.getJSONObject(alarmPosition);
        } catch(JSONException e){
            sharedArray = new JSONArray();
            jsonAlarm = new JSONObject();
        }

        alarmName = (TextView) findViewById(R.id.activity_alarm_name);
        alarmName.setText(jsonAlarm.optString("direction"));

        alarmDays = (TextView) findViewById(R.id.activity_alarm_days);
        alarmDays.setOnClickListener(daysListener);

        typeString = getResources().getStringArray(R.array.types_array);

        alarmType = (TextView) findViewById(R.id.activity_alarm_type);
        alarmType.setText(typeString[jsonAlarm.optInt("type")]);
        alarmType.setOnClickListener(typeListener);

        alarmTone = (TextView) findViewById(R.id.activity_alarm_tone);
        alarmTone.setText(jsonAlarm.optString("tone"));
        alarmTone.setOnClickListener(toneListener);

        cancelButton = (Button) findViewById(R.id.cancel);
        cancelButton.setOnClickListener(cancelListener);

        doneButton = (Button) findViewById(R.id.done);
        doneButton.setOnClickListener(doneListener);

        daysString = getResources().getStringArray(R.array.days_array);
        String daysMessage = "";
        try{
            if(jsonAlarm.getJSONArray("days").length() == 0){
                daysMessage = "Never";
            } else if(jsonAlarm.getJSONArray("days").length() == 7) {
                daysMessage = "Every day";
            } else {
                JSONArray arrayAux = jsonAlarm.getJSONArray("days");
                daysMessage = daysMessage.concat(daysString[arrayAux.getInt(0)]);
                int arrayLength = arrayAux.length();
                for (int i = 1; i < arrayLength; i++) {
                    daysMessage = daysMessage.concat(", ");
                    daysMessage = daysMessage.concat(daysString[arrayAux.getInt(i)]);
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        alarmDays.setText(daysMessage);

        volumeBar = (DiscreteSeekBar) findViewById(R.id.volume_seekbar);
        volumeBar.setProgress(jsonAlarm.optInt("volume"));

        distanceBar = (DiscreteSeekBar) findViewById(R.id.distance_seekbar);
        distanceBar.setProgress(jsonAlarm.optInt("distance"));
        distanceBar.setOnProgressChangeListener(onProgressChangeListener);

        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if(android.os.Build.VERSION.SDK_INT >= 21) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }

    }



    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }

        if(jsonAlarm.optDouble("latitude") == 0){
            Location location = getLocation();
            if(location != null){
                map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
            }
        } else {
            map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(jsonAlarm.optDouble("latitude"), jsonAlarm.optDouble("longitude"))));
        }

        circle = googleMap.addCircle(new CircleOptions()
                .center(googleMap.getCameraPosition().target)
                .radius(jsonAlarm.optInt("distance"))
                .strokeWidth(0f)
                .fillColor(0x556dceff));

        map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener(){
            @Override
            public void onCameraMove(){
                circle.setCenter(googleMap.getCameraPosition().target);
            }
        });
    }

    public DiscreteSeekBar.OnProgressChangeListener onProgressChangeListener = new DiscreteSeekBar.OnProgressChangeListener() {
        @Override
        public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
            circle.setRadius((double) value);
        }

        @Override
        public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

        }
    };

    public static class AlarmTone {
        String title;
        Uri uri;

        AlarmTone(String a, Uri b){
            title = a;
            uri = b;
        }
    }

    private List<AlarmTone> toneList = new ArrayList<>();
    private String selectedTone = "";
    private MediaPlayer player;

    public View.OnClickListener toneListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog dialog = new Dialog(AlarmActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.custom_tonepicker);

            RingtoneManager manager = new RingtoneManager(AlarmActivity.this);
            manager.setType(RingtoneManager.TYPE_ALARM);
            Cursor cursor = manager.getCursor();
            while (cursor.moveToNext()) {
                AlarmTone aux = new AlarmTone(cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX), manager.getRingtoneUri(cursor.getPosition()));
                toneList.add(aux);
            }

            RadioGroup radioG = (RadioGroup) dialog.findViewById(R.id.radio_group);

            for(int i = 0 ; i < toneList.size() ; i++){
                RadioButton radioB = new RadioButton(AlarmActivity.this);
                radioB.setText(toneList.get(i).title);
                radioG.addView(radioB);
            }

            radioG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int childCount = group.getChildCount();
                    for (int x = 0; x < childCount; x++) {
                        RadioButton btn = (RadioButton) group.getChildAt(x);
                        if (btn.getId() == checkedId) {
                            selectedTone = btn.getText().toString();
                            Log.w("ONTONECHANGED", selectedTone);

                            Uri notification = toneList.get(x).uri;
                            if(player != null){
                                player.stop();
                                player.release();
                            }
                            player = MediaPlayer.create(getApplicationContext(), notification);
                            float log1=(float)(Math.log(10-jsonAlarm.optInt("volume"))/Math.log(10));
                            player.setVolume(1-log1,1-log1);
                            player.start();
                        }
                    }
                }
            });

            Button dialogButton = (Button) dialog.findViewById(R.id.dialog_done3);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(player != null){
                        player.stop();
                        player.release();
                    }
                    alarmTone.setText(selectedTone);
                    Log.w("TONESELECT", selectedTone);
                    try{
                        jsonAlarm.put("tone", selectedTone);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };


    public View.OnClickListener typeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog dialog = new Dialog(AlarmActivity.this);
            dialog.setContentView(R.layout.custom_typepicker);

            int currentType;
            try{
                currentType = jsonAlarm.getInt("type");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            if(currentType == 0){
                RadioButton a = (RadioButton) dialog.findViewById(R.id.radio_0);
                a.setChecked(true);
            } else if(currentType == 1) {
                RadioButton b = (RadioButton) dialog.findViewById(R.id.radio_1);
                b.setChecked(true);
            } else {
                RadioButton c = (RadioButton) dialog.findViewById(R.id.radio_2);
                c.setChecked(true);
            }

            Button dialogButton = (Button) dialog.findViewById(R.id.dialog_done2);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RadioButton a = (RadioButton) dialog.findViewById(R.id.radio_0);
                    RadioButton b = (RadioButton) dialog.findViewById(R.id.radio_1);
                    int copyInt = 0;
                    if(a.isChecked()){
                        alarmType.setText("Sound");
                    } else if(b.isChecked()) {
                        alarmType.setText("Vibration");
                        copyInt = 1;
                    } else {
                        alarmType.setText("Vibration and Sound");
                        copyInt = 2;
                    }

                    try{
                        jsonAlarm.put("type", copyInt);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    public Button.OnClickListener cancelListener = new View.OnClickListener() {
        public void onClick(View v) {
            finish();
        }
    };

    public Button.OnClickListener doneListener = new View.OnClickListener() {
        public void onClick(View v) {
            try{
                jsonAlarm.put("direction", alarmName.getText());
                jsonAlarm.put("volume", volumeBar.getProgress());
                jsonAlarm.put("distance", distanceBar.getProgress());
                jsonAlarm.put("latitude", googleMap.getCameraPosition().target.latitude);
                jsonAlarm.put("longitude", googleMap.getCameraPosition().target.longitude);

                Log.w("Save", jsonAlarm.toString());
                sharedArray.put(alarmPosition, jsonAlarm);
                sharedJson.put(getString(R.string.saved_json_array), sharedArray);
                sharedString = sharedJson.toString();
                sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), getApplicationContext().MODE_PRIVATE);
                Log.w("SharedPref", sharedPref.toString());
                editor = sharedPref.edit();

                editor.clear();
                editor.putString(getString(R.string.saved_json), sharedString);
                editor.commit();

                Intent serviceIntent = new Intent(getApplicationContext(), TrackingService.class);
                serviceIntent.putExtra(MainActivity.EXTRA_JSON, sharedString);
                startService(serviceIntent);
            } catch(JSONException e){
                Log.w("SAVE_ERROR", sharedString);
            }

            finish();
        }
    };

    public View.OnClickListener daysListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog dialog = new Dialog(AlarmActivity.this);
            dialog.setContentView(R.layout.custom_datepicker);


            JSONArray checkboxStatus;
            ArrayList<Integer> repeatDays = new ArrayList<>();
            try{
                checkboxStatus = jsonAlarm.getJSONArray("days");
                for(int i = 0; i < checkboxStatus.length(); i++){
                    repeatDays.add(checkboxStatus.getInt(i));
                }
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            dialogChecks = new ArrayList<>();
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_1));
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_2));
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_3));
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_4));
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_5));
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_6));
            dialogChecks.add((CheckBox) dialog.findViewById(R.id.checkbox_7));

            for(int i = 0; i < repeatDays.size(); i++){
                dialogChecks.get(repeatDays.get(i)).setChecked(true);
            }

            Button dialogButton = (Button) dialog.findViewById(R.id.dialog_done);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONArray savingDays = new JSONArray();
                    String daysMessage = "";
                    int dialogChecksSize = dialogChecks.size();
                    for(int i = 0; i < dialogChecksSize; i++){
                        if(dialogChecks.get(i).isChecked()){
                            if(!daysMessage.equals("")){
                                daysMessage = daysMessage.concat(", ");
                            }
                            daysMessage = daysMessage.concat(daysString[i]);
                            savingDays.put(i);
                        }
                    }
                    alarmDays.setText(daysMessage);
                    try{
                        jsonAlarm.put("days", savingDays);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    public Location getLocation() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location lastKnownLocationGPS;
        Location returnLocation = null;
        if (locationManager != null) {
            try {
                lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastKnownLocationGPS != null) {
                    returnLocation = lastKnownLocationGPS;
                } else {
                    returnLocation =  locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
            } catch (SecurityException sec){

            }
        } else {
            return null;
        }
        return returnLocation;
    }

}