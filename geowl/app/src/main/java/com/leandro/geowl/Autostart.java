package com.leandro.geowl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by 54pccc on 12/12/2017.
 */

public class Autostart extends BroadcastReceiver {

    public void onReceive(Context context, Intent arg1)
    {
        Intent serviceIntent = new Intent(context, TrackingService.class);
        Log.e("INTENT", "Autostart");
        context.startService(serviceIntent);
    }
}
